# House Price Visualization
##### Data visualisation for showing the change in house prices.

Tech Stack:

- Numpy with Pandas

- Flask

- AngularJS

- D3

This is a visulisation created using a data source from data.go.uk:
https://data.gov.uk/dataset/land-registry-monthly-price-paid-data

The repo only contains an aggregated version of the data as the original csv is
3 GB!

If you wish to use a more up to date version of the data then you can download
it from the url above and place it in the data folder. (It must be called
pp-complete.csv). From there you can run `python wrangle_data.py` which will
generate a new output file which can be used in the app.

Installation and quick start:

- `git clone`

- `cd house_prices`

- `virtualenv env`

- `source env/bin/activate`

- `pip install -r requirements.txt`

- `cd app && python app.py`


Visit 127.0.0.1:5000 to view app!