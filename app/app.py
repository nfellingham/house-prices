#!flask/bin/python
import json
import pandas as pd
from flask import Flask, request, Response, jsonify, render_template


app = Flask(__name__, static_url_path='')


class DataAccess(object):
    """
    A simple class for loading the data csv into a pandas dataframe and
    returning native python data types.
    """

    def __init__(self):
        self._data = self._fetch_data()
        self.county_list = self._fetch_county_list()

    @staticmethod
    def _fetch_data():
        return pd.read_csv('../data/output.csv')

    def _fetch_county_list(self):
        return self._data['county'].unique().tolist()

    def get_county_preview(self, county):
        county_data = self._data[self._data['county'] == county][
            ['date', 'mean', 'len']
        ]
        return json.loads(county_data.to_json(orient="records"))

    def get_county_data(self, county):
        county_data = self._data[self._data['county'] == county]
        return json.loads(county_data.to_json(orient="records"))

# Singleton for accessing data.
data_access = DataAccess()



@app.route('/house_prices/', methods=['GET'])
def house_prices():
    """
    A house price api endpoint which requires a county to be passed in and will
    return house price stats for the given county.
    """
    county = request.args.get('county', None)
    if not county:
        return Response('"county" parameter required!', status=400)
    if county not in data_access.county_list:
        return Response('County not found.', status=400)

    preview = request.args.get('preview', None) in ['true', '1']
    if preview:
        return jsonify({'results': data_access.get_county_preview(county)})
    else:
        return jsonify({'results': data_access.get_county_data(county)})


@app.route('/counties/', methods=['GET'])
def counties_list():
    """
    An api endpoint which will return a list of all available counties.
    """
    return jsonify(
        {'results': data_access.county_list}
    )


@app.route('/')
def index():
    """
    For serving the angular app.
    """
    return app.send_static_file('index.html')


if __name__ == '__main__':
    app.run(host='localhost', debug=True, port=4000)
