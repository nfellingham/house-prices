var vizApp = angular.module('vizApp', ['rzModule']);

vizApp

    /**
    * Property data services for connecting to flask API's
    */
    .factory('propertyData', function($http, $q) {
        var countyCache = {};
        /**
        * Function for return a list of house price data points for a specific
        * county. Each data point looks like:
        * {
        *       "county": "AVON",
        *       "date": "1995-01",
        *       "mean": 61186.5642256903,
        *       "perc_0": 8000,
        *       "perc_1": 14966,
        *        ...
        *       "perc_99": 202719.9999999998,
        *       "price": 833
        *   },
        *
        * The perc_<n> values are the percentiles.
        */
        function getCounty(county, preview) {
            preview = typeof preview !== 'undefined' ? preview : false;

            var deferred = $q.defer();
            var config = {params: {
                'county': county,
                'preview': preview
            }};

            if (countyCache[county + preview]) {
                deferred.resolve(countyCache[county + preview])
            } else {
                $http.get('/house_prices', config).then(
                    function(data) {
                        // Parse the date here.
                        var parseDate = d3.time.format("%Y-%m").parse;
                        data.data.results.forEach(function (d) {
                            d.mean = +d.mean;
                            d.date = parseDate(d.date);
                        });

                        countyCache[county + preview] = data.data.results;
                        deferred.resolve(data.data.results);
                    },
                    function(error) {
                        deferred.reject(error);
                    }
                );
            }

            return deferred.promise;
        }

        /**
        * Returns a list of counties from the API which are available for
        * querying
        */
        function getCountyList() {
            var deferred = $q.defer();

            $http.get('/counties').then(
                function(data) {
                    deferred.resolve(data.data.results);
                },
                function(error) {
                    deferred.reject(error);
                }
            );

            return deferred.promise;
        }

        return {
            getCounty: getCounty,
            getCountyList: getCountyList
        };

    })
    .filter('titleCase', function() {
        //http://stackoverflow.com/questions/24039226/
        //angularjs-format-text-return-from-json-to-camel-case
        return function(input) {
            input = input || '';
            return input.replace(/\w\S*/g, function(txt){
                return txt.charAt(0).toUpperCase() +
                  txt.substr(1).toLowerCase();
            });
        };
    })
    /**
    * Main app controller for interacting with data in Angular
    */
    .controller('MainCtrl', function ($scope, $http, propertyData, $timeout, $anchorScroll) {
        $scope.chartApi = {
            percentilesHidden: true
        };
        $scope.chartData = null;

        $scope.countySearch = '';
        $scope.countyList = [];
        $scope.currentCounty = null;

        $scope.sliderValue = 50;

        $scope.sliderOptions = {
            floor: 0,
            ceil: 100,
            disabled: true
        };

        /**
        * Initialization of data on scope.
        */
        propertyData.getCountyList().then(
            function(data) {
                $scope.countyList = data;

                if (data.indexOf("CITY OF BRISTOL") != -1) {
                    $scope.currentCounty = "CITY OF BRISTOL";
                } else {
                    $scope.currentCounty = data[0]
                }
                return propertyData.getCounty($scope.currentCounty);
            }
        ).then(
            function(data) {
                $scope.chartData = data;
                $scope.chartApi.initData($scope.chartData);
                $anchorScroll(data[0].county)
            }
        );

        /**
        * Load county into the chart.
        */
        $scope.loadCounty = function(county) {
            $scope.currentCounty = county;
            propertyData.getCounty($scope.currentCounty).then(
                function(data) {
                    $scope.hidePreviewCounty();
                    $scope.chartData = data;
                    $scope.chartApi.loadData($scope.chartData);
                }

            )
        };

        /**
        * Turn on and off the percentiles on the chart. Also disable slider.
        */
        $scope.togglePercentiles = function() {
            $scope.sliderOptions.disabled = !$scope.sliderOptions.disabled;
            $scope.chartApi.percentilesHidden = $scope.sliderOptions.disabled;

            $scope.chartApi.movePercentile(
                $scope.sliderValue
            )
        };


        $scope.lockedCounty = null;
        $scope.toggleLockedPreview = function(county) {
            if ($scope.lockedCounty === county) {
                $scope.lockedCounty = null;
                $scope.hidePreviewCounty(true);
            } else {
                $scope.lockedCounty = county;
                $scope.previewCounty(county, true)
            }
        };

        /**
        * Show county preview on chart.
        */
        var showPreview;
        $scope.previewCounty = function(county, lock) {
            showPreview = true;
            if (lock || !$scope.lockedCounty) {
                propertyData.getCounty(county, true).then(
                    function (data) {
                        if (showPreview) {
                            $scope.chartApi.drawPreview(data, lock)
                        }
                    }
                );
            }
        };

        /**
        * Hide county preview
        */
        $scope.hidePreviewCounty = function(lock) {
            if (lock || !$scope.lockedCounty) {
                showPreview = false;
                $scope.chartApi.hidePreview()
            }
        };

        /**
        * Send slider data to d3.
        */
        var sliderTimeout;
        $scope.$watch('sliderValue', function(now, was) {
            if (now !== was) {
                if (sliderTimeout) {
                    $timeout.cancel(sliderTimeout);
                }
                sliderTimeout = $timeout(function() {
                    $scope.chartApi.movePercentile(now);
                }, 210);
            }
        })

    })

    /**
    * Directive for drawing chart in D3
    */
    .directive('chart', function ($parse) {
        var drawD3 = function (chart, api) {
            // The data for the main line
            var chartData = null;
            // The data for the preview / comparison line.
            var previewData = null;

            var margin = {top: 20, right: 20, bottom: 30, left: 60},
                width = 860 - margin.left - margin.right,
                height = 500 - margin.top - margin.bottom;

            var x = d3.time.scale()
                .range([0, width]);

            var y = d3.scale.linear()
                .range([height, 0]);

            var areaFillColor = d3.scale.linear().domain([0, 50, 100]).range(
                ["#c7f5ff", "#ffffff", "#ffacb0"]
            );

            var areaLineColor = d3.scale.linear().domain([0, 50, 100]).range(
                ["#3498db", "#d5d5d5", "#db5252"]
            );

            var xAxis = d3.svg.axis()
                .scale(x)
                .ticks(d3.time.year, 1)
                .orient("bottom");

            var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left");

            var getPercentileArea = function(perc) {
                return d3.svg.area()
                    .interpolate("bundle")
                    .x(function (d) { return x(d.date); })
                    .y0(function (d) { return y(d.mean); })
                    .y1(function (d) { return y(d[perc]); });
            };

            var percentilesClosed = d3.svg.area()
                .interpolate("bundle")
                .x(function (d) { return x(d.date); })
                .y0(function (d) { return y(d.mean); })
                .y1(function (d) { return y(d.mean); });

            var mean = d3.svg.line()
                .interpolate("bundle")
                .x(function (d) {
                    return x(d.date);
                })
                .y(function (d) {
                    return y(d.mean);
                });

            var preview = d3.svg.line()
                .interpolate("bundle")
                .x(function (d) {
                    return x(d.date);
                })
                .y(function (d) {
                    return y(d.mean);
                });

            var svg = chart.append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            var currentPerc = 50;

            var transformY = function() {
                var chartDataMax = d3.max(chartData, function (d) {
                    return d3.max([d.mean, d['perc_' + currentPerc]]);
                });
                var previewDataMax = previewData ? d3.max(
                  previewData, function (d) { return d.mean; }) : 0;

                var y_max = d3.max([chartDataMax, previewDataMax]);
                y.domain([0, y_max]);

                d3.selectAll(".line")
                    .transition()
                    .duration(1000)
                    .attr("d", mean);

                d3.selectAll(".preview")
                    .transition()
                    .duration(2000)
                    .attr("d", mean);

                if (api.percentilesHidden) {
                    d3.selectAll(".area")
                    .transition()
                    .attr("d", percentilesClosed)
                    .style("fill", areaFillColor(currentPerc))
                    .style("fill", areaFillColor(currentPerc))
                    .duration(1000);
                } else {
                    d3.selectAll(".area")
                        .transition()
                        .style("fill", areaFillColor(currentPerc))
                        .style("stroke", areaLineColor(currentPerc))
                        .duration(1000)
                        .attr("d", getPercentileArea('perc_' + currentPerc));
                }

                svg.select(".y")
                 .transition().duration(1500).ease("sin-in-out")
                 .call(yAxis)
                 .selectAll("text")
                 .attr("transform", "translate(-5, -10) rotate(-45)");
            };

            /**
            * Initialisation for first load of data into the chart.
            */
            api.initData = function(data) {
                chartData = data;

                x.domain(d3.extent(data, function (d) {
                    return d.date;
                }));
                y.domain([0, d3.max(data, function (d) {
                    return d.mean;
                })]);

                svg.append("path")
                    .datum(data)
                    .attr("class", "area")
                    .attr("d", percentilesClosed);

                svg.append("path")
                    .datum(data)
                    .attr("class", "line")
                    .attr("d", mean);

                svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis);

                svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis)
                    .selectAll("text")
                    .attr("transform", "translate(-5, -10) rotate(-45)")
                    .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("y", 6)
                    .attr("dy", ".71em")
                    .style("text-anchor", "end")
                    .text("Average House Price (£)");
            };

            /**
            * Called when loading subsequent data into D3
            */
            api.loadData = function(data) {
                chartData = data;

                d3.selectAll(".preview")
                    .transition()
                    .duration(2500)
                    .attr("d", mean);

                svg.select(".area").remove();
                svg.append("path")
                    .datum(data)
                    .attr("class", "area")
                    .attr("d", percentilesClosed);

                svg.select(".line").remove();
                svg.append("path")
                    .datum(data)
                    .attr("class", "line")
                    .attr("d", mean);

                transformY();
            };

            /**
            * Used to move the percentile line. Optional parameter hide used to
             * hide the percentile line.
            */
            api.movePercentile = function(perc) {
                currentPerc = perc;
                transformY();
            };


            api.drawPreview = function(data, isFixed) {
                api.hidePreview();
                previewData = data;

                svg.append("path")
                    .datum(data)
                    .attr("class", "preview")
                    .style("stroke-dasharray", ("3"))
                    .attr("d", mean);

                if (isFixed) {
                    transformY();
                }
            };

            api.hidePreview = function() {
                previewData = null;
                d3.selectAll(".preview")
                    .remove();
                transformY();
            };

        };

        return {
            restrict: 'E',
            scope: {data: '=chartData', api: '=chartApi'},
            replace: false,
            link: function (scope, element, attrs) {
                var chart = d3.select(element[0])
                    .attr("class", "chart");
                drawD3(chart, scope.api);
            }
        };
    });