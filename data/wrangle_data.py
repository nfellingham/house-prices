import os

import numpy as np
import pandas as pd


FILEOUT = 'output.csv'


def perc_func_creator(perc):
    def func(val):
        return np.percentile(val, perc)

    func.__name__ = 'perc_' + str(perc)
    return func


def main():
    data_files = os.listdir(os.getcwd())
    if 'pp-complete.csv' in data_files:
        d_file = 'pp-complete.csv'
    elif 'sample_pp-complete.csv' in data_files:
        print "Only sample data found, download the complete dataset from: " + \
              "https://data.gov.uk/dataset/land-registry-monthly-price-paid-data"
        d_file = 'sample_pp-complete.csv'
    else:
        return

    # https://www.gov.uk/guidance/about-the-price-paid-data
    column_names = [
        'uuid',
        'price',
        'date',
        'postcode',
        'property_type',
        'new',
        'duration',
        'paon',
        'saon',
        'street',
        'locality',
        'town_city',
        'district',
        'county',
        'ppdcategory',
        'record_status',
    ]
    # price date new county
    use_columns = [1, 2, 5, 13]

    # https://data.gov.uk/dataset/land-registry-monthly-price-paid-data
    data = pd.read_csv(d_file, names=column_names, usecols=use_columns)

    # Get just year and quarter for group by
    data['date'] = data['date'].apply(lambda x: x[:7])

    # Group by date and county then add aggregation stats.
    group_by = ['date', 'county']

    base_aggs = [len, np.mean]
    # Create a function to return the percentile from 0 - 100 and name them,
    # note there is definitely a better way of doing this!
    perc_aggs = []
    for i in range(101):
        perc_aggs.append(perc_func_creator(i))

    grouped = data.groupby(group_by, as_index=False).agg(
        base_aggs + perc_aggs
    )

    # Flatten index columns
    grouped.reset_index(inplace=True)
    # Columns are messed up because of multi index
    split_index = len(group_by)
    grouped.columns = grouped.columns.get_level_values(0)[:split_index].tolist() + \
                      grouped.columns.get_level_values(1)[split_index:].tolist()

    # Output data
    print 'Saving to file: {}'.format(FILEOUT)
    grouped.to_csv(FILEOUT, index=False)


if __name__ =="__main__":
    main()
